//
//  AppDelegate.swift
//  yourPrice
//
//  Created by Productions on 10/29/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseCore
import FacebookCore
import FacebookLogin
import Google
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate  , GIDSignInDelegate{
   
    
  
    

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.sharedManager().enable = true
        FIRApp.configure()
        GIDSignIn.sharedInstance().clientID = "711619320584-sd63i9lkmbudb4m8mf3nckpqtdd75e7c.apps.googleusercontent.com"
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        
        if configureError != nil{
            print(configureError?.localizedDescription)
        }
        GIDSignIn.sharedInstance().delegate = self
        return SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url as URL,
                                                                   sourceApplication: sourceApplication,
                                                                   annotation: annotation)
        
        let facebookDidHandle = SDKApplicationDelegate.shared.application(application, open: url as URL, sourceApplication: sourceApplication, annotation: annotation)
        
        return googleDidHandle || facebookDidHandle
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        AppEventsLogger.activate(application)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
    }
}

