//
//  ServicesCell.swift
//  yourPrice
//
//  Created by Productions on 10/31/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit

class ServicesCell: UITableViewCell {

    @IBOutlet weak var serviceImage : UIImageView!
    @IBOutlet weak var serviceLbl  : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
