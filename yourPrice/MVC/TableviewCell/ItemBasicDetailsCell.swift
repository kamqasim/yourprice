//
//  ItemBasicDetailsCell.swift
//  yourPrice
//
//  Created by Productions on 11/1/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit

class ItemBasicDetailsCell: UITableViewCell {

    @IBOutlet weak var serviceImage : UIImageView!
    @IBOutlet weak var serviceDetailsLbl  : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
