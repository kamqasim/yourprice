//
//  CategoryModel.swift
//  yourPrice
//
//  Created by Productions on 11/5/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import RealmSwift

class CategoryModel {
    
    var catetegoryID = ""
    var categoryName = ""
    var categoryPic  = ""
    var categoryImagePath = ""
    
    func updateModelWithJSON(json:JSON){
            catetegoryID = (json["cat_id"].stringValue)
            categoryName = (json["cat_name"].stringValue)
            categoryPic = (json["cat_pic"].stringValue)
            categoryImagePath = (json["pic_path"].stringValue)
    }
    
    
    static func HomeAPI(params: [String : Any] , completion: @escaping (_ categoryList : [CategoryModel]? , _ productList : [ProductModel]?) -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        API.sharedInstance.executeAPI(type: .gethome, method: .post, params: params) { (status, response, message) in
            
            SVProgressHUD.dismiss()
            var categoryList = [CategoryModel]()
            var productList = [ProductModel]()
            if status == .success{
                //SVProgressHUD.showSuccess(withStatus: message)
   
                response["category"].array?.forEach({ (categoryItem) in
                    let category = CategoryModel()
                    category.updateModelWithJSON(json: categoryItem)
                    categoryList.append(category)
                })
                response["products"].array?.forEach({ (productItem) in
                    let product = ProductModel()
                    product.updateModelWithJSON(json: productItem)
                    productList.append(product)
                })
                DispatchQueue.main.async {
                    completion(categoryList,productList)
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
    
}
