//
//  UserModel.swift
//  yourPrice
//
//  Created by Productions on 11/4/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftyJSON
import SVProgressHUD

class UserModel: Object {
    
    
    override class func primaryKey() -> String? {
        return "userid"
    }
    
    @objc dynamic var userName: String = ""
    @objc dynamic var userid  : String = ""
    @objc dynamic var firstName: String = ""
    @objc dynamic var email: String = ""
    @objc dynamic var phoneNumber: String = ""
    @objc dynamic var address  : String = ""
    @objc dynamic var orderTime : String = ""
    
    func updateModelWithJSON(json:JSON){
        
        // let detail = json.dictionaryValue
        self.userid = (json["userid"].stringValue)
        self.firstName = (json["name"].stringValue)
        self.email = (json["email"].stringValue)
        self.phoneNumber = (json["phone"].stringValue)
        self.address   = (json["address"].stringValue)
        self.userName = (json["name"].stringValue)
        self.orderTime = (json["order_date"].stringValue)
        
    }
    
    static func getCurrentUser() -> UserModel? {
        let realm = try! Realm()
        if realm.objects(UserModel.self).count > 0 {
            return realm.objects(UserModel.self).first
        }
        return nil
    }
    
    static func addModel(json : JSON) {
        let user = UserModel()
        user.updateModelWithJSON(json: json)
        
        let realm = try! Realm()
        if realm.objects(UserModel.self).filter("userid == '\(user.userid)'").count > 0 {
            let model = realm.objects(UserModel.self).filter("userid == '\(user.userid)'").first
            model?.updateModelWithJSON(json: json)
        }
        else {
            realm.add(user)
        }
    }
    
    //MARK: - APIs
    static func loginAPI(params: [String : Any] , completion: @escaping (_ status: Bool) -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        var apiStatus  : Bool = false
        API.sharedInstance.executeAPI(type: .login, method: .post, params: params) { (status, response, message) in
            SVProgressHUD.dismiss()
            if status == .success{
                apiStatus = true
                SVProgressHUD.showSuccess(withStatus: message)
                let realm = try! Realm()
                try! realm.write {
                    print(response)
                    UserModel.addModel(json: response["user"])
                }
                DispatchQueue.main.async {
                    completion(apiStatus)
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
    
    static func sighnUpAPI(params: [String : Any] , completion: @escaping () -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        API.sharedInstance.executeAPI(type: .updateProfile, method: .post, params: params) { (status, response, message) in
            
            SVProgressHUD.dismiss()
            if status == .success{
                SVProgressHUD.showSuccess(withStatus: message)
                let realm = try! Realm()
                try! realm.write {
                    print(response)
                    UserModel.addModel(json: response["user"])
                }
                DispatchQueue.main.async {
                    completion()
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
    
    
    static func sendEmailAPI(params: [String : Any] , completion: @escaping (_ verifyCode : String) -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        API.sharedInstance.executeAPI(type: .verifyCode, method: .post, params: params) { (status, response, message) in
            
            SVProgressHUD.dismiss()
            var code : String = ""
            if status == .success{
                SVProgressHUD.showSuccess(withStatus: message)
                code = response["code"].stringValue
                DispatchQueue.main.async {
                    completion(response["code"].stringValue)
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
    
    static func resetPasswordAPI(params: [String : Any] , completion: @escaping () -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        API.sharedInstance.executeAPI(type: .updatePassword, method: .post, params: params) { (status, response, message) in
            
            SVProgressHUD.dismiss()
            if status == .success{
                SVProgressHUD.showSuccess(withStatus: message)
                let realm = try! Realm()
                try! realm.write {
                    print(response)
                   // UserModel.addModel(json: response["user"])
                }
                DispatchQueue.main.async {
                    completion()
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
    
    static func updateProfileAPI(params: [String : Any] , completion: @escaping (_ status: Bool ) -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        
        API.sharedInstance.executeAPI(type: .updateProfile, method: .post, params: params) { (status, response, message) in
           var apiStatus : Bool = false
            SVProgressHUD.dismiss()
            if status == .success{
                SVProgressHUD.showSuccess(withStatus: message)
                let realm = try! Realm()
                try! realm.write {
                    print(response)
                    apiStatus = true
                    //UserModel.addModel(json: response["user"])
                }
                DispatchQueue.main.async {
                    completion(apiStatus)
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
}
