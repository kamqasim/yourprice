//
//  ProductModel.swift
//  yourPrice
//
//  Created by Productions on 11/5/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD

class ProductModel{
    
    var productID               : String = ""
    var productName             : String = ""
    var productPic              : String = ""
    var productPicPath          : String = ""
    var productCat              : String  = ""
    var productDesc             : String = ""
    var productStartdate        : String = ""
    var productEnddate          : String = ""
    var productStartTimestamp   : String = ""
    var productEndTimestamp     : String = ""
    
    func updateModelWithJSON(json:JSON){
        
        productID = (json["product_id"].stringValue)
        productName = (json["product_name"].stringValue)
        productPic = (json["product_pic"].stringValue)
        productPicPath = (json["product_pic_path"].stringValue)
        productCat = (json["product_cat"].stringValue)
        productDesc = (json["product_desc"].stringValue)
        productStartdate = (json["product_startdate"].stringValue)
        productEnddate = (json["product_enddate"].stringValue)
        productStartTimestamp = (json["product_start_timestamp"].stringValue)
        productEndTimestamp = (json["product_end_timestamp"].stringValue)
    }
    
    
    static func getItemByCatAPI(params: [String : Any] , completion: @escaping ( _ productList : [ProductModel]?) -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        API.sharedInstance.executeAPI(type: .getItemByCat, method: .post, params: params) { (status, response, message) in
            
            SVProgressHUD.dismiss()
            var productList = [ProductModel]()
            if status == .success{
               // SVProgressHUD.showSuccess(withStatus: message)
                
                response["products"].array?.forEach({ (productItem) in
                    let product = ProductModel()
                    product.updateModelWithJSON(json: productItem)
                    productList.append(product)
                })
                DispatchQueue.main.async {
                    completion(productList)
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
    
    
    static func orderNowAPI(params: [String : Any] , completion: @escaping ( _ productList : [ProductModel]?) -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        API.sharedInstance.executeAPI(type: .addOrder, method: .post, params: params) { (status, response, message) in
            
            SVProgressHUD.dismiss()
            var productList = [ProductModel]()
            if status == .success{
                SVProgressHUD.showSuccess(withStatus: message)
                
//                response["products"].array?.forEach({ (productItem) in
//                    let product = ProductModel()
//                    product.updateModelWithJSON(json: productItem)
//                    productList.append(product)
//                })
                DispatchQueue.main.async {
                    completion(productList)
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }

}
