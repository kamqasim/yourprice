//
//  Randum Model.swift
//  yourPrice
//
//  Created by Productions on 11/4/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SwiftyJSON
import SVProgressHUD

class RandumModel {

    //MARK: - APIs
    static func getDataAPI(params: [String : Any]? , completion: @escaping () -> Void){
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.clear)
        API.sharedInstance.executeAPI(type: .gethome, method: .post, params: nil) { (status, response, message) in
            
            SVProgressHUD.dismiss()
            if status == .success{
                let realm = try! Realm()
                try! realm.write {
                    print(response)
                   // UserModel.addModel(json: response["user"])
                }
                DispatchQueue.main.async {
                    completion()
                }
            }
            else if status == .failure {
                
                SVProgressHUD.showError(withStatus: message)
            }
            else if status == .authError {
                
                SVProgressHUD.showError(withStatus: message)
            }
        }
    }
}
