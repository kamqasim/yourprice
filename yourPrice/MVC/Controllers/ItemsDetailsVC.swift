//
//  ItemsDetailsVC.swift
//  yourPrice
//
//  Created by Productions on 11/6/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import AlamofireImage
import CircleProgressView
import SVProgressHUD

class ItemsDetailsVC: UIViewController {
    
    var selectedItem = ProductModel()
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var descriptiontextView: UITextView!
    @IBOutlet weak var enterPriceTextField: UITextField!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var timerView: UIView!
    
    @IBOutlet weak var roundedView: UIView!
  
    fileprivate var timer: Timer?
    
    
    var timeEnd : Date?
    var header : HeaderView?
     var EndDate = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let siplit =   selectedItem.productName.components(separatedBy: " ")
        print(siplit)
        if siplit.count >= 3{
            self.title = siplit[0] + " " + siplit[1] + " " + siplit[2]
        }else if siplit.count == 2{
            self.title = siplit[0] + " " + siplit[1]
        }else if siplit.count == 1{
            self.title = siplit[0]
        }else{
            self.title = selectedItem.productName
        }
        
        print(selectedItem.productEnddate)
        print(selectedItem.productStartdate)
        enterPriceTextField.setBottomBorder()
        addShadowWithBorder(shadowView:self.shadowView , color: UIColor.gray, opacity: 0.7, shadowRadius:10.0)
        roundedView.cornerRadius = 15
        TimerStarting()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let imagePath = selectedItem.productPicPath + selectedItem.productPic
        self.nameLbl.text = selectedItem.productName
        print(selectedItem.productDesc.htmlToAttributedString)
        self.descriptiontextView.text = selectedItem.productDesc.htmlToString
        
        let attributes = [
            NSAttributedStringKey.font : Theme.segoeUItext,
            NSAttributedStringKey.foregroundColor : UIColor.black
            ] as [NSAttributedStringKey : Any]
        
        let description = NSAttributedString(string: selectedItem.productDesc.htmlToString, attributes: attributes)
        
        self.descriptiontextView.attributedText = description
        if !imagePath.isEmpty{
            let url = URL(string: imagePath)!
            let placeholderImage = UIImage(named: "default-placeholder")!
            let filter = ScaledToSizeWithRoundedCornersFilter(
                size: imageView.frame.size,
                radius: 0.0
            )
            imageView.af_setImage(
                withURL: url,
                placeholderImage: placeholderImage,
                filter: filter,
                imageTransition: .flipFromTop(0.2)
            )
        }
        imageView.contentMode = .scaleAspectFit
    }
    
    @IBAction func CheckOutAction(_ sender: Any) {
        if let price = enterPriceTextField.text{
            if price != ""{
                let vc = AppInternalNetwork.getCheckOutVC() as! CheckOutVC
                print(selectedItem.productName)
                vc.selectedItem = self.selectedItem
                vc.price = enterPriceTextField.text!
                self.pushToVC(vc: vc)
            }else{
                SVProgressHUD.showError(withStatus: "Pleace Enter Price First")
            }
        }
    }
}

