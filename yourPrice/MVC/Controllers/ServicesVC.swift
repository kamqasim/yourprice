//
//  ServicesVC.swift
//  yourPrice
//
//  Created by Productions on 10/31/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import AlamofireImage
import RealmSwift

class ServicesVC: UIViewController {

    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var randumCollectionView: UICollectionView!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var productsLbl: UILabel!
    
    var page : Int = 1
    var limit : Int = 20
    let realm = try! Realm()
    private let refreshControl = UIRefreshControl()
    var categoryList: [CategoryModel] = [] {
        didSet {
            self.categoryCollectionView.delegate = self
            self.categoryCollectionView.dataSource = self
            self.categoryCollectionView.reloadData()
        }
    }
    var currentUser = UserModel.getCurrentUser()
    
    var productList: [ProductModel] = [] {
        didSet {
            self.randumCollectionView.delegate = self
            self.randumCollectionView.dataSource = self
            self.randumCollectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        randumCollectionView.layer.cornerRadius = 5
        randumCollectionView.clipsToBounds = true
        // Do any additional setup after loading the view.
        getHomeDataRequest(params: ["page":page,"limit":limit])
        self.setUpTableView()
    }
    
    @IBAction func pressMeLoadData(_ sender: Any) {
        getHomeDataRequest(params: ["page":page,"limit":limit])
    }
    
    func setUpTableView() {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            randumCollectionView.refreshControl = refreshControl
        } else {
             randumCollectionView.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshTaskList(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        let strokeTextAttributes = [
            NSAttributedStringKey.strokeColor : UIColor.black,
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.strokeWidth : -2.0,
            NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 18)
            ] as [NSAttributedStringKey : Any]
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Task List ...", attributes: strokeTextAttributes)
    }
    
    @objc private func refreshTaskList(_ sender: Any) {
        // Fetch Weather Data
        getHomeDataRequest(params: ["page":page,"limit":limit])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
//       if categoryList.count == 0 {
//            getHomeDataRequest(params: ["page":page,"limit":limit])
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getHomeDataRequest(params:[String:Any]){
        CategoryModel.HomeAPI(params: params) { (categoryList, productList) in
            if categoryList?.isEmpty == false{
                if let categoryList = categoryList{
                    if categoryList.count > 0 {
                        self.categoryList.removeAll()
                        categoryList.forEach({ (category) in
                            self.categoryList.append(category)
                        })
                    }
                }
            }
            if productList?.isEmpty == false{
                if let productList = productList{
                    self.refreshControl.endRefreshing()
                    productList.forEach({ (product) in
                        self.productList.append(product)
                    })
                }
            }
        }
    }
}

extension ServicesVC : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.5
        let yourHeight = yourWidth
        if collectionView == categoryCollectionView{
           return CGSize(width: yourWidth, height: yourHeight + 40)
        }else{
            return CGSize(width: collectionView.bounds.width/2.05, height: yourHeight + 130)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
//
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == categoryCollectionView{
            return 0
        }
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == categoryCollectionView{
            return 4
        }
        return 5
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        if collectionView == categoryCollectionView{
            if (self.categoryList.count == 0) {
                self.categoryCollectionView.isHidden = true
                self.categoryLbl.isHidden = true
            } else {
                self.refreshControl.endRefreshing()
                self.categoryLbl.isHidden = false
                self.categoryCollectionView.isHidden = false
                self.categoryCollectionView.restore()
            }
            return categoryList.count
        }else{
            if (self.productList.count == 0) {
                self.randumCollectionView.isHidden = true
                self.productsLbl.isHidden = true
            } else {
                self.refreshControl.endRefreshing()
                self.randumCollectionView.isHidden = false
                self.productsLbl.isHidden = false
                self.randumCollectionView.restore()
            }
            return productList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == categoryCollectionView{
            let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
            let category = categoryList[indexPath.row]
            cell.detailsLbl.text = category.categoryName
            if category.categoryImagePath.isEmpty == false{
              
                let imagePath = category.categoryImagePath + category.categoryPic
                cell.imageView.tag = indexPath.row
                if !imagePath.isEmpty{
                    let url = URL(string: imagePath)!
                    let placeholderImage = UIImage(named: "default-placeholder")!
                    let filter = ScaledToSizeWithRoundedCornersFilter(
                        size: cell.imageView.frame.size,
                        radius: 0.0
                    )
                    cell.imageView.af_setImage(
                        withURL: url,
                        placeholderImage: placeholderImage,
                        filter: filter,
                        imageTransition: .flipFromTop(0.2)
                    )
                }
            }
            
            cell.imageView.clipsToBounds = true
            cell.imageView.layer.cornerRadius = 5
            cell.categoryRoundedView.layer.borderWidth = 5
            cell.categoryRoundedView.layer.cornerRadius = 5
            cell.categoryRoundedView.clipsToBounds = true
            cell.categoryRoundedView.borderColor = UIColor(red: 226/255, green: 227/255, blue: 229/255, alpha: 0.3)
            
            return cell
        }else{
            let cell = randumCollectionView.dequeueReusableCell(withReuseIdentifier: "randumCell", for: indexPath) as! randumCell
            let product = productList[indexPath.row]
            cell.nameLbl.text = product.productName
            
            let imagePath = product.productPicPath + product.productPic
            //print(imagePath)
            cell.imageView.tag = indexPath.row
            if !imagePath.isEmpty{
                let url = URL(string: imagePath)!
                let placeholderImage = UIImage(named: "default-placeholder")!
                let filter = ScaledToSizeWithRoundedCornersFilter(
                    size: cell.imageView.frame.size,
                    radius: 0.0
                )
                cell.imageView.af_setImage(
                    withURL: url,
                    placeholderImage: placeholderImage,
                    filter: filter,
                    imageTransition: .flipFromTop(0.2)
                )
            }
            cell.detailsLbl.text = product.productCat
            cell.imageView.layer.cornerRadius = 5
            cell.randumbackgroundView.layer.cornerRadius = 5
            cell.imageView.clipsToBounds = true

            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == categoryCollectionView{
            let category = categoryList[indexPath.row]
            let vc = AppInternalNetwork.getItemsBasicDetailsVC() as! ItemsBasicDetailsVC
            vc.hidesBottomBarWhenPushed = true
                vc.selectedItem  = category.categoryName
                self.pushToVC(vc: vc)
        }else{
            let selectedItem = productList[indexPath.row]
            let vc = AppInternalNetwork.getItemsDetailsVC() as! ItemsDetailsVC
            vc.selectedItem = selectedItem
            vc.hidesBottomBarWhenPushed = true
            self.pushToVC(vc: vc)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == productList.count - 1 {  //numberofitem count
            updateNextSet(index: indexPath.row)
            print(indexPath.row)
        }
    }
    
    func updateNextSet(index:Int){
        if productList.count % 20 == 0 {
            page = page + 1
        }
        getHomeDataRequest(params: ["page":page,"limit":limit])
        print("On Completetion")
        //requests another set of data (20 more items) from the server.
    }
}
extension UICollectionView {
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
    }
    
    func restore() {
        self.backgroundView = nil
    }
}
