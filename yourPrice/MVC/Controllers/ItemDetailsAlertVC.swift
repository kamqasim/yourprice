////
////  ItemDetailsAlertVC.swift
////  yourPrice
////
////  Created by Productions on 11/1/18.
////  Copyright © 2018 Productions. All rights reserved.
////
//
//import UIKit
//class ItemDetailsAlertVC: UIViewController {
//    
//    @IBOutlet weak var imageView: UIImageView!
//    @IBOutlet weak var caroselView: iCarousel!
//    @IBOutlet weak var productNameLbl: UILabel!
//    @IBOutlet weak var productTypelbl: UILabel!
//    @IBOutlet weak var productBrandLbl: UILabel!
//    @IBOutlet weak var availableColorlbl: UILabel!
//    
//    var images : [UIImage] = [#imageLiteral(resourceName: "electronic"),#imageLiteral(resourceName: "electronic"),#imageLiteral(resourceName: "electronic"),#imageLiteral(resourceName: "electronic"),#imageLiteral(resourceName: "electronic")]
//    
//    var selectedItem =  ""
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.title = "Coffee Machine"
//        
//        images = [
//            UIImage(named: "coffe1.png")!,
//            UIImage(named: "coffe2.png")!,
//            UIImage(named: "coffe3.png")!,
//            UIImage(named: "coffe4.png")!,
//            UIImage(named: "coffe5.png")!,
//            UIImage(named: "coffe6.png")!,
//            UIImage(named: "coffe7.png")!,
//            UIImage(named: "coffe8.png")!
//        ]
//        caroselView.type = .linear
//        caroselView.scrollSpeed = 0.1
//        caroselView.autoscroll = -0.1 
//        caroselView.contentMode = .scaleAspectFit
//        caroselView.isPagingEnabled = true
//        
//        // Do any additional setup after loading the view.
//    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        caroselView.delegate = self
//        caroselView.dataSource = self
//        
//        caroselView.reloadData()
//    }
//}
//
//extension ItemDetailsAlertVC : iCarouselDelegate , iCarouselDataSource{
//    
//    
//    func numberOfItems(in carousel: iCarousel) -> Int {
//        return  images.count
//    }
//    
//    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
//        var thisimageView : UIImageView!
//        if view == nil{
//            thisimageView = UIImageView(frame: CGRect(x: 0  , y: 0, width: self.imageView.frame.width, height: self.imageView.frame.height))
//            
//            thisimageView.contentMode = .scaleAspectFit
//        }else{
//            thisimageView = view as?  UIImageView
//        }
//        thisimageView.image = images[index]
//        return thisimageView
//    }
//    
//    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
//        let item = images[index]
//        print(item)
//    }
//    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
//    {
//        switch (option)
//        {
//            
//        case .wrap:
//            return value * 0.1
//            
//        default:
//            return value
//        }
//    }
//}
