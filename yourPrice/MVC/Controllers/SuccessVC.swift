//
//  SuccessVC.swift
//  yourPrice
//
//  Created by Productions on 11/11/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit

class SuccessVC: UIViewController {

    @IBOutlet weak var messageLbl: UILabel!
    var currentUser = UserModel.getCurrentUser()
    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = currentUser{
            
            messageLbl.attributedText = NSAttributedString(string: "Hi " + user.userName + ", we're getting ready your order.\nPlease check your email for your order summary.")
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func backToHome(_ sender: Any) {
        self.backToRootTappedPop()
    }
    
}
