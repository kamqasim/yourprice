//
//  CheckOutVC.swift
//  yourPrice
//
//  Created by Productions on 11/8/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift
import AlamofireImage

class CheckOutVC: UIViewController  , UITextViewDelegate{

    
    @IBOutlet weak var deliveryTimeLbl: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productnameLbl: UILabel!
    @IBOutlet weak var productAvaiableLbl: UILabel!
    @IBOutlet weak var productinStockLbl: UILabel!
    @IBOutlet weak var orderedProductNameLbl: UILabel!
    @IBOutlet weak var productnamePriceLbl: UILabel!
    @IBOutlet weak var productVatPriceLbl: UILabel!
    @IBOutlet weak var productShippingPriceLbl: UILabel!
    @IBOutlet weak var productTotalPriceLbl: UILabel!
    
    @IBOutlet weak var productAddressLbl: UILabel!
    @IBOutlet weak var productAddressTextField: UITextField!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var yourCartLbl: UILabel!
    @IBOutlet weak var productDetailsView: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var productDetailsshedowView: UIView!
    @IBOutlet weak var orderBtn: UIButton!
    
    let realm = try! Realm()
    var  placeholder = "Comments (Optional)"
    var selectedItem : ProductModel  = ProductModel() {
        willSet{
        }
    }
    var price : String = ""
    var vat : Int = 15
    var currentUser = UserModel.getCurrentUser()
    var orderTime = ""
    var dateFormater = DateFormatter()
    var date = Date()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        productnameLbl.font = Theme.robotoBoldtext
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        textView.text = placeholder
        textView.textColor = UIColor.lightGray
        textView.font = Theme.segoeUItext
        textView.returnKeyType = .done
        textView.delegate = self
        addShadowWithBorder(shadowView: self.productDetailsView, color: UIColor.black, opacity: 0.5, shadowRadius: 1.0)
        addShadowWithBorder(shadowView: self.commentView, color: UIColor.black, opacity: 0.5, shadowRadius: 1.0)
        addShadowWithBorder(shadowView: self.addressView, color: UIColor.black, opacity: 0.5, shadowRadius: 1.0)
          dataPlacement()
    }
    
    func dataPlacement(){
        
        productnameLbl.text  = selectedItem.productName
        productAvaiableLbl.text = "Available"
        productinStockLbl.text = "IN STOCK"
        orderedProductNameLbl.text = selectedItem.productName
        productnamePriceLbl.text = price + " AED"
        productVatPriceLbl.text = "15 AED"
        productShippingPriceLbl.text = "1 AED"
        let vatCalculation = (Double(price)! * 100)/Double(100 - vat)
        productTotalPriceLbl.text = String(format: "%.2f", vatCalculation + 1.0) + " AED"
        setCartLbl() 
        setAddressLbl()
        let imagePath = selectedItem.productPicPath + selectedItem.productPic
        if !imagePath.isEmpty{
            let url = URL(string: imagePath)!
            let placeholderImage = UIImage(named: "default-placeholder")!
            let filter = ScaledToSizeWithRoundedCornersFilter(
                size: self.productImageView.frame.size,
                radius: 0.0
            )
            self.productImageView.af_setImage(
                withURL: url,
                placeholderImage: placeholderImage,
                filter: filter,
                imageTransition: .flipFromTop(0.2)
            )
        }
        productImageView.contentMode = .scaleAspectFit
        let formatter = DateFormatter()
        formatter.dateStyle = DateFormatter.Style.full
        formatter.timeStyle = .short
        formatter.timeZone = .current
        let dateString = formatter.string(from: date.dayAfter.dayAfter.dayAfter).components(separatedBy: ",")
       
        let dateString1 = formatter.string(from: date.dayAfter.dayAfter.dayAfter.dayAfter.dayAfter).components(separatedBy: ",")
        print(dateString1)
        
        deliveryTimeLbl.text = "Expected Delivery : " + dateString[0] + dateString[1] + " to " + dateString1[0] + dateString1[1]
    }
    
    func setCartLbl(){
        
        let attribute1 = [NSAttributedStringKey.font: Theme.robotoBoldtext ,NSAttributedStringKey.foregroundColor: UIColor.black]
        let attribute2 = [ NSAttributedStringKey.font: Theme.robotoRegulartext ,NSAttributedStringKey.foregroundColor: UIColor.gray  ]
        yourCartLbl.attributedText = attributed(totalString: ["Your Cart"," (1)"], attributes: [attribute1,attribute2])
    }
    
    func setAddressLbl(){
        
        let attribute1 = [NSAttributedStringKey.font: Theme.robotoBoldtext ,NSAttributedStringKey.foregroundColor: UIColor.black]
        let attribute2 = [ NSAttributedStringKey.font: Theme.robotoAddressSmalltext ,NSAttributedStringKey.foregroundColor: UIColor.black  ]
         if let user = UserModel.getCurrentUser(){
            productAddressTextField.attributedText = attributed(totalString: [user.address], attributes: [attribute2])
        }
    }
    
    func attributed(totalString:[String],attributes:[[NSAttributedStringKey: Any]])-> NSMutableAttributedString{
        
        let totalStringMuted = NSMutableAttributedString()
        
        for i in 0..<totalString.count {
            totalStringMuted.append(NSMutableAttributedString(string: totalString[i], attributes: attributes[i]))
        }
        return  totalStringMuted
    }
    
    @IBAction func orderNowAction(_ sender: Any) {
        
        if let user = UserModel.getCurrentUser(){
            dateFormater.dateFormat = "dd-MM-yyyy"
            orderTime = dateFormater.string(from: Date())
            if user.orderTime  == orderTime{
               SVProgressHUD.showInfo(withStatus: "You Can place One Order In a Day")
            }else{
                let timeStamp = Int(Date().timeIntervalSince1970 * 1000)
                let params = [
                    "prod_id": "\(selectedItem.productID)","order_price":"\(productTotalPriceLbl.text!)","prod_quantity":"\(1)","order_desc":"\(textView.text!)","order_date":"\(orderTime)","timestamp":"\(timeStamp)","email":"\(user.email)","name":"\(user.userName)","mobile":"\(user.phoneNumber)","address":"\(productAddressTextField.text!)"
                    
                    ] as [String: Any]
                orderNowAPI(params: params)
                print(params)
            }
        }
    }
    
func orderNowAPI(params:[String: Any]){
    ProductModel.orderNowAPI(params: params) { (product) in
        print("Success")
     
        if let user = self.currentUser{
            print(user.orderTime)
            try! self.realm.write {
                print(user.orderTime)
                user.orderTime = self.orderTime
                self.realm.add(user, update: true)
            }
        }
        let vc = AppInternalNetwork.getSuccessVC() as! SuccessVC
        self.pushToVC(vc: vc)
    }
}
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholder {
            textView.text = ""
            textView.textColor = UIColor.black
            textView.font = Theme.segoeUItext
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = UIColor.lightGray
            textView.font = Theme.segoeUItext
        }
    }
}
extension Date {
    static var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: Date().noon)!
    }
    static var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: Date().noon)!
    }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}
