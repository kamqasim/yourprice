//
//  ItamsBasicDetailsVC.swift
//  yourPrice
//
//  Created by Productions on 11/1/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController
import AlamofireImage

class ItemsBasicDetailsVC: UIViewController {
    
    
    @IBOutlet weak var categoryListCollectionView: UICollectionView!
    @IBOutlet weak var productLbl: UILabel!
    
    var page : Int = 1
    var limit : Int = 10
    var selectedItem = ""
    private let refreshControl = UIRefreshControl()
    var productList: [ProductModel] = [] {
        didSet {
            self.categoryListCollectionView.delegate = self
            self.categoryListCollectionView.dataSource = self
            self.categoryListCollectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = selectedItem
        getCategoryListRequest(params: ["page":page, "limit":limit,"category":selectedItem])
        setUpTableView()
    }
    
    
    func setUpTableView() {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            categoryListCollectionView.refreshControl = refreshControl
        } else {
            categoryListCollectionView.addSubview(refreshControl)
        }
        
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshTaskList(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.darkGray
        let strokeTextAttributes = [
            NSAttributedStringKey.strokeColor : UIColor.black,
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.strokeWidth : -2.0,
            NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 18)
            ] as [NSAttributedStringKey : Any]
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Task List ...", attributes: strokeTextAttributes)
    }
    
    @objc private func refreshTaskList(_ sender: Any) {
        // Fetch Weather Data
        getCategoryListRequest(params: ["page":page, "limit":limit,"category":selectedItem])
    }
    
    func getCategoryListRequest(params:[String:Any]){
        ProductModel.getItemByCatAPI(params: params) { (productList) in
            if productList?.isEmpty == false{
                if let productList = productList{
                    if productList.count > 0 {
                        self.refreshControl.endRefreshing()
                        productList.forEach({ (product) in
                            self.productList.append(product)
                        })
                    }
                }
                print(self.productList.count)
            }
        }
    }
}

extension ItemsBasicDetailsVC : UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.5
        let yourHeight = yourWidth
        return CGSize(width: collectionView.bounds.width/2.05, height: yourHeight + 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    //
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (self.productList.count == 0) {
            self.categoryListCollectionView.isHidden = true
            self.productLbl.isHidden = true
        } else {
            self.refreshControl.endRefreshing()
            self.productLbl.isHidden = false
            self.categoryListCollectionView.isHidden = false
            self.categoryListCollectionView.restore()
        }
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = categoryListCollectionView.dequeueReusableCell(withReuseIdentifier: "randumCell", for: indexPath) as! randumCell
        
        let product = productList[indexPath.row]
        cell.nameLbl.text = product.productName
        let imagePath = product.productPicPath + product.productPic
        if !imagePath.isEmpty{
            let url = URL(string: imagePath)!
            let placeholderImage = UIImage(named: "default-placeholder")!
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: cell.imageView.frame.size,
                radius: 0.0
            )
            cell.imageView.af_setImage(
                withURL: url,
                placeholderImage: placeholderImage,
                filter: filter,
                imageTransition: .flipFromTop(0.2)
            )
        }
        cell.detailsLbl.text = product.productCat
        cell.imageView.layer.cornerRadius = 5
        cell.randumbackgroundView.layer.cornerRadius = 5
        cell.imageView.clipsToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedItem = productList[indexPath.row]
        let vc = AppInternalNetwork.getItemsDetailsVC() as! ItemsDetailsVC
        vc.selectedItem = selectedItem
        self.pushToVC(vc: vc)
        print(selectedItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == productList.count - 1 {  //numberofitem count
            updateNextSet(index: indexPath.row)
            print(indexPath.row)
        }
    }
    
    func updateNextSet(index:Int){
        if productList.count % 10 == 0 {
            page = page + 1
        }
        getCategoryListRequest(params: ["page":page,"limit":limit])
        print("On Completetion")
        //requests another set of data (20 more items) from the server.
    }
}
