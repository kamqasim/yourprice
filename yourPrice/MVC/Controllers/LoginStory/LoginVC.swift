//
//  ViewController.swift
//  yourPrice
//
//  Created by Productions on 10/29/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SVProgressHUD
import RealmSwift
import FacebookLogin
import FBSDKLoginKit
import GoogleSignIn

class LoginVC: UIViewController ,GIDSignInDelegate, GIDSignInUIDelegate {
    
    
    @IBOutlet weak var usernamTextField: MyCustomTextField!
    @IBOutlet weak var passwordTextField: MyCustomTextField!
    @IBOutlet weak var loginBtn: MyCustombutton!
    @IBOutlet weak var signUpBtn: MyCustombutton!
    @IBOutlet weak var fbSighnUpBtn: UIButton!
    @IBOutlet weak var googleSignUpBtn: UIButton!
    
    var dict : [String : AnyObject]!
   
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        usernamTextField.text = "kamqasim1@gmail.com"
        passwordTextField.text = "yourPrice39))"
        self.hideKeyboardWhenTappedAround()
        
        googleSignUpBtn.addTarget(self, action: #selector(googleLoginButtonClicked), for: .touchUpInside)
        fbSighnUpBtn.addTarget(self, action: #selector(loginButtonClicked), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
       //when google login button clicked
    @objc func googleLoginButtonClicked() {
        GIDSignIn.sharedInstance().signIn()
    }
   
    //when Fb login button clicked
    @objc func loginButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.email,.publicProfile], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                FBSDKLoginManager().logOut()
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                self.getFBUserData()
            }
        }
    }
    
    //function is fetching the user data
    func getFBUserData(){
        let parameters = ["fields": "email,picture.type(large),name,gender,age_range,cover,timezone,verified,updated_time,education,religion,friends"]
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    var email = ""
                    var id  = ""
                    
                    if let userData = result as? [String:AnyObject] {
                        if let thisemail = userData["name"] as? String , let thisid = userData["name"] as? String{
                            email = thisemail
                             id = thisid
                            let params = [
                                "email":"\(email)","password":""
                                ,"status":"f"] as [String: Any]
                            
                            print(params)
                            self.loginRequest(params: params)
                        }
                    }
                    print(result!)
                }
            })
        }
    }
    
    func validateInputs(email:String ,password: String ) -> String {
        
        if !self.isValidEmail(testStr: email){
            return "Invalid Email"
        }
        
        if !self.isValidPassword(testStr: password) {
            return "The Password Must Be At Least 6 Characters"
        }
        return ""
    }
    
    func loginRequestValidation(){
        
        var error = checkFieldStringValues(stringValues: [usernamTextField.text!,passwordTextField.text!], stringErrors: ["Please Enter UserName   ", "Please Enter Password"])
        
        if error == ""{
            
            error = validateInputs(email: usernamTextField.text!, password:  passwordTextField.text!)
        }
        
        if error != ""{
            SVProgressHUD.showInfo(withStatus: error)
            return
        }
     
        let params = [
            "email":"\(usernamTextField.text!)","password":"\(passwordTextField.text!)"
            ,"status":"n"] as [String: Any]
        
        print(params)
        loginRequest(params: params)
    }
    
    func loginRequest(params:[String: Any]){
        UserModel.loginAPI(params: params) { (apiStaus) in
            if apiStaus{
                print("Success")
                let vc = AppInternalNetwork.getAppTabBarController() as! AppTabBarController
                vc.navigationController?.isNavigationBarHidden = false
                self.present(vc, animated: true, completion: nil)
            }else{
                let vc = AppInternalNetwork.getSignUpVC() as! SignUpVC
                vc.pushToVC(vc: vc)
            }
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        
        self.loginRequestValidation()
    }
    
    @IBAction func forgotPassWordAction(_ sender: Any) {

        let vc = AppInternalNetwork.getSendEmailVC() as! SendEmailVC
        self.pushToVC(vc: vc)
    }
    
    @IBAction func signUPAction(_ sender: Any) {
        let vc = AppInternalNetwork.getSignUpVC() as! SignUpVC
        self.pushToVC(vc: vc)
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print("Google Sing In didSignInForUser")
        if let error = error {
            print(error.localizedDescription)
            return
        }
        guard let authentication = user.authentication else { return }
        if (error == nil) {
            // Perform any operations on signed in user here.
            if let email = user.profile.email{
                let params = [
                    "email":"\(email)","password":""
                    ,"status":"g"] as [String: Any]
                print(params)
                loginRequest(params: params)
            }
        } else {
            print("\(error.localizedDescription)")
        }
    }
    // Start Google OAuth2 Authentication
    func sign(_ signIn: GIDSignIn?, present viewController: UIViewController?) {
        
        // Showing OAuth2 authentication window
        if let aController = viewController {
            present(aController, animated: true) {() -> Void in }
        }
    }
    // After Google OAuth2 authentication
    func sign(_ signIn: GIDSignIn?, dismiss viewController: UIViewController?) {
        // Close OAuth2 authentication window
        dismiss(animated: true) {() -> Void in }
    }
}


