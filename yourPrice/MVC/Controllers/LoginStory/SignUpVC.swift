//
//  SignUpVC.swift
//  yourPrice
//
//  Created by Productions on 11/4/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit
import SVProgressHUD

class SignUpVC: UIViewController {
 
    @IBOutlet weak var usernamTextField: MyCustomTextField!
    @IBOutlet weak var emailTextField: MyCustomTextField!
    @IBOutlet weak var phoneTextField: MyCustomTextField!
    @IBOutlet weak var addressTextField: MyCustomTextField!
    @IBOutlet weak var passowrdTextField: MyCustomTextField!
    @IBOutlet weak var confirmPasswordTextField: MyCustomTextField!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        usernamTextField.text = "Muhammad"
        emailTextField.text = "kamqasim1@gmail.com"
        phoneTextField.text = "00971568973908"
        addressTextField.text = "Silicon Oasis ,Dubai,UAE"
        passowrdTextField.text = "yourPrice390)"
        confirmPasswordTextField.text = "yourPrice390)"
    }
    
    
    func validateInputs(email:String ,password: String ) -> String {
        
        if !self.isValidEmail(testStr: email){
            return "Invalid Email"
        }
        
        if !self.isValidPassword(testStr: password) {
            return "The Password Must Be At Least 6 Characters"
        }
        return ""
    }
    
    func loginRequestValidation(){
        
        var error = checkFieldStringValues(stringValues: [usernamTextField.text!,emailTextField.text!,passowrdTextField.text!,confirmPasswordTextField.text!], stringErrors: ["Please Enter UserName   ","Please Enter Email", "Please Enter Confirm Password", "Please Enter Password"])
        
        if error == ""{
            
            error = validateInputs(email: emailTextField.text!, password:  passowrdTextField.text!)
        }
        
        if error != ""{
            SVProgressHUD.showInfo(withStatus: error)
            return
        }
        
        let params = [
            "name": "\(usernamTextField.text!)","email":"\(emailTextField.text!)"
            ,"phone":"\(phoneTextField.text!)","password":confirmPasswordTextField.text!,"address":addressTextField.text!] as [String: Any]
        print(params)
        signUpRequest(params: params)
    }
    
    func setLeftImage(button:UIButton,left:Bool ,image:UIImage?){
        let imageView = UIImageView()
        imageView.image = image
        imageView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        button.imageEdgeInsets = UIEdgeInsets(top: 40, left: 50, bottom: 0, right: 0)
        button.contentHorizontalAlignment = .center
        button.contentMode = .left
    }
    
    func signUpRequest(params:[String: Any]){
        UserModel.sighnUpAPI(params: params) {
            print("Success")
            
            let vc = AppInternalNetwork.getAppTabBarController() as! AppTabBarController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        loginRequestValidation()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.popVc()
    }
}
