//
//  randumCell.swift
//  yourPrice
//
//  Created by Productions on 11/1/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit

class randumCell: UICollectionViewCell {
    
    
    @IBOutlet weak var randumbackgroundView: UIView!
    @IBOutlet weak var imageBackGroundView: DesignableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
}
