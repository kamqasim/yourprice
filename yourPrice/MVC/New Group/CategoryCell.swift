//
//  CategoryCell.swift
//  yourPrice
//
//  Created by Productions on 11/1/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    
    @IBOutlet weak var categorybackgroundView: UIView!
    
    @IBOutlet weak var categoryRoundedView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var detailsLbl: UILabel!
}
