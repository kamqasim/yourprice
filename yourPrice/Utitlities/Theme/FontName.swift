//
//  Font.swift
//  yourPrice
//
//  Created by Productions on 11/11/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import Foundation
import UIKit

class FontName{
    
    static var segueUI : String = "SegoeUI"
    static var robotoRegular : String = "Roboto-Regular"
    static var robotoMedium : String = "Roboto-Medium"
    static var robotoBold : String = "Roboto-Bold"
    
    
    
    static var smallFontSize : CGFloat  = 13.0
    static var normalFontSize : CGFloat  = 15.0
    static var largeFontSize : CGFloat  = 20.0
    
}
