//
//  Theme.swift
//  yourPrice
//
//  Created by Productions on 11/7/18.
//  Copyright © 2018 Productions. All rights reserved.
//

import Foundation
import UIKit

class Theme{
    
    static var segoeUItext : UIFont = UIFont(name: FontName.segueUI, size: FontName.normalFontSize)!
    static var robotoRegulartext : UIFont = UIFont(name: FontName.robotoRegular, size: FontName.normalFontSize)!
    static var robotoMediumtext : UIFont = UIFont(name: FontName.robotoMedium, size: FontName.normalFontSize)!
    static var robotoBoldtext : UIFont = UIFont(name: FontName.robotoBold, size: FontName.normalFontSize)!
    
    
    static var robotoAddressSmalltext : UIFont = UIFont(name: FontName.robotoRegular, size: FontName.smallFontSize)!

}
