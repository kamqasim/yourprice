//
//  AppInternalNetwork.swift
//  yourPrice
//
//  Created by Productions on 11/1/18.
//  Copyright © 2018 Productions. All rights reserved.
//
import Foundation
import UIKit



enum Storyboard: String {
    
    case launchScreenStory = "LaunchScreen"
    case loginStoryboard = "Login"
    case mainStoryBoard = "MainStoryboard"
    case profileStory = "ProfileStoryBoard"

}

class AppInternalNetwork: NSObject {
    
    private static let launchScreenStoryboard = UIStoryboard(name: Storyboard.launchScreenStory.rawValue, bundle: nil)
    private static let loginStoryboard = UIStoryboard(name: Storyboard.loginStoryboard.rawValue, bundle: nil)
    private static let mainStoryBoard = UIStoryboard(name: Storyboard.mainStoryBoard.rawValue, bundle: nil)
    
    static var passwordChangeSuccessMessage = ""
    
    static var latitude : String = String()
    static var longitide : String = String()
    static var place     : String = String()
    
    static func getLoginVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
    }
    
    static func getSendEmailVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "SendEmailVC") as! SendEmailVC
    }
    
    
    static func getVerfiCodeVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "VerfiCodeVC") as! VerfiCodeVC
    }
    
    static func getResetPasswordVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
    }
    
    static func getSignUpVC() -> UIViewController {
        return loginStoryboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
    }
    
    static func getAppTabBarController() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "AppTabBarController") as! AppTabBarController
    }
    
    static func getServicesVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "ServicesVC") as! ServicesVC
    }
    
    static func getUpdateProfileVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
    }
    
    static func getItemsBasicDetailsVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "ItemsBasicDetailsVC") as! ItemsBasicDetailsVC
    }
    
    static func getItemsDetailsVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "ItemsDetailsVC") as! ItemsDetailsVC
    }
    
    static func getCheckOutVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
    }
    
    static func getSuccessVC() -> UIViewController {
        return mainStoryBoard.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
    }
    
    
    
}



