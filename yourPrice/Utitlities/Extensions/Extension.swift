//
//  Extension.swift
//  MenuThat
//
//  Created by Xtreme Hardware on 28/09/2017.
//  Copyright © 2017 kamqasim. All rights reserved.
//

import UIKit
import RealmSwift

extension UITextField {
    
    func modifyClearButton(with image : UIImage) {
        let clearButton = UIButton(type: .custom)
        clearButton.setImage(image, for: .normal)
        clearButton.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        clearButton.contentMode = .scaleAspectFit
        clearButton.addTarget(self, action: #selector(UITextField.clear(_:)), for: .touchUpInside)
        rightView = clearButton
        rightViewMode = .whileEditing
    }
    
    @objc func clear(_ sender : AnyObject) {
        self.text = ""
        sendActions(for: .editingChanged)
    }
}

extension UISearchBar {
    public func setSerchTextcolor(color: UIColor) {
        let clrChange = subviews.flatMap { $0.subviews }
        guard let sc = (clrChange.filter { $0 is UITextField }).first as? UITextField else { return }
        sc.textColor = color
    }
}

extension UITextField
{
    enum Direction
    {
        case Left
        case Right
    }
    
    func AddImage(direction:Direction,imageName:String,Frame:CGRect,backgroundColor:UIColor)
    {
        let View = UIView(frame: Frame)
        View.backgroundColor = backgroundColor
        
        let imageView = UIImageView(frame: Frame)
        imageView.image = UIImage(named: imageName)
        
        View.addSubview(imageView)
        
        if Direction.Left == direction
        {
            self.leftViewMode = .always
            self.leftView = View
        }
        else
        {
            self.rightViewMode = .always
            self.rightView = View
        }
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

public enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
extension UIImage {
    
    public func base64(format: ImageFormat) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = UIImagePNGRepresentation(self)
        case .jpeg(let compression): imageData = UIImageJPEGRepresentation(self, compression)
        }
        return imageData?.base64EncodedString()
    }
}
extension UIViewController{
    
    func getDate(date: String) -> Date{
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        var inputDate = dateFormatter.date(from: date)
        return inputDate!
    }
    
    func addShadowWithBorder(shadowView: UIView,color:UIColor,opacity:CGFloat,shadowRadius:CGFloat){
        shadowView.layer.shadowOpacity = 0.7
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        shadowView.layer.shadowRadius = shadowRadius
        shadowView.layer.shadowColor = color.cgColor
    }
    
    func showAlertDialog(title: String ,message: String ,completion: @escaping () -> Void )  {
        let alert = UIAlertController(title: title , message: message , preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addAction(UIAlertAction(title: "Purchase", style: .default, handler: { (action: UIAlertAction!) in
        //            completion()
        //            alert.dismiss(animated: true, completion: nil)
        //        }))
        alert.addAction(UIAlertAction(title: "Thanks", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func popBackAction(){
        
        let Ok =  UIAlertAction(title: "Yes", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
            
        }
        
        let Cancel =  UIAlertAction(title: "No", style: .destructive) { (action) in
            
        }
        
        self.AlertToSave(alertActions: [Ok,Cancel], message: "Are you sure? changes have not been saved.", title: "Alert" )
    }
    
    
    func setscolorfontSizeAttributedString(strings:[String],founts: [UIFont],color: [UIColor]) -> NSAttributedString{
        
        let myString : NSMutableAttributedString = NSMutableAttributedString()
        for i in 0...strings.count - 1 {
            myString.append(attributedString(string: strings[i],color:color[i], font: founts[i]))
        }
        return myString
    }
    
    func attributedString(string:String,color:UIColor,font: UIFont)->NSAttributedString{
        let myAttribute1 = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: color]
        return NSMutableAttributedString(string: string, attributes: myAttribute1)
    }
    
    func setBorderAndCornerRadius(layer: CALayer, width: CGFloat, radius: CGFloat,color : UIColor ) {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    
    func popVc() {
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func backToRootTappedPop(){
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    func backTappedPop(){
        DispatchQueue.main.async {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func pushToVC(vc: UIViewController) {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func popBack<T:UIViewController>(toControllerType : T.Type){
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers{
            viewControllers.forEach({ (currentViewController) in
                if currentViewController.isKind(of: toControllerType){
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                }
            })
        }
    }
    
    func encodeBase64(image: UIImage) -> String{
        let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        return strBase64
    }
    
    
    func isValidName(in text: String) -> Bool
    {
        do
        {
            let regex = try NSRegularExpression(pattern: "^[0-9a-zA-Z\\_]{7,18}$", options: .caseInsensitive)
            if regex.matches(in: text, options: [], range: NSMakeRange(0, text.count)).count > 0 {return true}
        }
        catch {}
        return false
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPassword(testStr: String) -> Bool {
        if testStr.count >= 6 {
            return true
        }
        return false
    }
    
    func isValidCode(testStr: String) -> Bool {
        if testStr.count >= 4 {
            return true
        }
        return false
    }
    
    func isValidNameCount(testStr: String) -> Bool {
        print(testStr)
        if testStr.count >= 6 {
            return true
        }
        return false
    }
    
    func isValidContact(testStr : String  ) -> Bool{
        let contactRegex = "(\\+[0-9]+[\\- \\.]*)?" + "(\\([0-9]+\\)[\\- \\.]*)?"
            + "([0-9][0-9\\- \\.]+[0-9])"
        //let contactRegex = "/^(\\+\\d{1,3}[- ]?)?\\d{10}$/"
        
        let contacttest = NSPredicate(format:"SELF MATCHES %@", contactRegex)
        return contacttest.evaluate(with: testStr)
    }
    
    //String values validation
    func checkFieldStringValues(stringValues : [String] , stringErrors : [String]) -> String {
        var index = 0
        if(stringValues.count != 0 && stringErrors.count != 0){
            for values in stringValues{
                
                if (values == ""){
                    return stringErrors[index]
                }
                index = index + 1
            }
        }
        return ""
    }
    // SetPlaceHolder onto TextField
    func setPlaceHolder(textFileds : [UITextField],placeHolder: [String]){
        for i in 0...textFileds.count - 1 {
            
            textFileds[i].placeholder = placeHolder[i]
        }
    }
    
    func AlertToSave(alertActions:[UIAlertAction], message : String ,title: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        for action in alertActions{
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func logout(){
        
        let vc = AppInternalNetwork.getLoginVC() as! LoginVC
        let appDelegate = UIApplication.shared.delegate
        
        
        let nav = UINavigationController.init(rootViewController: vc)
        nav.navigationBar.isHidden = true
        nav.interactivePopGestureRecognizer?.isEnabled = false
        appDelegate?.window??.rootViewController = nav
//        let realm = try! Realm()
//        try! realm.write {
//            realm.deleteAll()
//        }
    }

}




extension UINavigationController {
    
    func backToViewController(vc: Any) {
        for element in viewControllers as Array {
            if "\(type(of: element)).Type" == "\(type(of: vc))" {
                self.popToViewController(element, animated: true)
                break
            }
        }
    }
    
}
extension UINavigationController {
    var rootViewController : UIViewController? {
        return viewControllers.first
    }
}

extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.clear.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the amount of nanoseconds from another date
    func nanoseconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.nanosecond], from: date, to: self).nanosecond ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        var result: String = ""
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if seconds(from: date) > 0 { return "\(seconds(from: date))" }
        if days(from: date)    > 0 { result = result + " " + "\(days(from: date)) D" }
        if hours(from: date)   > 0 { result = result + " " + "\(hours(from: date)) H" }
        if minutes(from: date) > 0 { result = result + " " + "\(minutes(from: date)) M" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))" }
        return ""
    }
}

extension ItemsDetailsVC{
    
    func generateCurrentTimeStamp () -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if selectedItem.productEndTimestamp != ""{
            if let date =  Double(selectedItem.productEndTimestamp){
                EndDate = date
                let date = Date(timeIntervalSince1970:EndDate)
                return (formatter.string(from: date) as NSString) as String
            }
        }
        return ""
    }
    
    func TimerStarting(){
        
        let endDate = generateCurrentTimeStamp()
        if endDate != "" {
            timeEnd = Date(timeInterval: endDate.toDate(format: "yyyy-MM-dd HH:mm:ss").timeIntervalSince(Date()), since: Date())
            addTimerView()
        }else{
            print("Expire Offer Date The Product")
        }
    }
    
    func addTimerView(){
        
        let height = timerView.frame.height
        header = HeaderView(frame: CGRect(x:0, y: 0,width : timerView.frame.width, height : height))
        self.timerView.addSubview(header!)
        updateView()
    }
    
    func updateView() {
        setTimeLeft()
        // Start timer
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.setTimeLeft), userInfo: nil, repeats: true)
    }
    
    @objc func setTimeLeft() {
        let timeNow = Date()
        
        
        if timeEnd?.compare(timeNow) == ComparisonResult.orderedDescending {
            
            let interval = timeEnd?.timeIntervalSince(timeNow)
            
            let days =  (interval! / (60*60*24)).rounded(.down)
            
            let daysRemainder = interval?.truncatingRemainder(dividingBy: 60*60*24)
            
            let hours = (daysRemainder! / (60 * 60)).rounded(.down)
            
            let hoursRemainder = daysRemainder?.truncatingRemainder(dividingBy: 60 * 60).rounded(.down)
            
            let minites  = (hoursRemainder! / 60).rounded(.down)
            
            let minitesRemainder = hoursRemainder?.truncatingRemainder(dividingBy: 60).rounded(.down)
            
            let scondes = minitesRemainder?.truncatingRemainder(dividingBy: 60).rounded(.down)
            
            header?.DaysProgress.setProgress(days/360, animated: false)
            header?.hoursProgress.setProgress(hours/24, animated: false)
            header?.minitesProgress.setProgress(minites/60, animated: false)
            header?.secondesProgress.setProgress(scondes!/60, animated: false)
            let formatter = NumberFormatter()
            formatter.minimumIntegerDigits = 2
            header?.valueDay.text = formatter.string(from: NSNumber(value:days))
            header?.valueHour.text = formatter.string(from: NSNumber(value:hours))
            header?.valueMinites.text = formatter.string(from: NSNumber(value:minites))
            header?.valueSeconds.text = formatter.string(from: NSNumber(value:scondes!))
        } else {
            header?.fadeOut()
        }
    }
}

extension String{
    func toDate(format : String) -> Date{
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)!
    }
}

extension UIView {
    func fadeIn() {
        // Move our fade out code from earlier
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0 // Instead of a specific instance of, say, birdTypeLabel, we simply set [thisInstance] (ie, self)'s alpha
        }, completion: nil)
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
        }, completion: nil)
    }
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue:      CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableButton: UIButton {
}

@IBDesignable
class DesignableLabel: UILabel {
}

extension UIView {
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
 
}
extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
}
extension Notification.Name {
    static let didReceiveData = Notification.Name("didReceiveData")
    static let didCompleteTask = Notification.Name("didCompleteTask")
    static let completedLengthyDownload = Notification.Name("completedLengthyDownload")
}
extension Date {
    func monthAsString() -> String {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("MMM")
        return df.string(from: self)
}
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
